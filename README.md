# clock

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### utools

打包后，在dist文件下面添加logo.png图片，然后在把index.html里面的路径都加上**./**

##### index.html样式内容，修改路径

```HTML
<!doctype html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="./favicon.ico">
    <title>clock</title>
    <script defer="defer" src="./js/chunk-vendors.b80f01f5.js"></script>
    <script defer="defer" src="./js/app.f019c923.js"></script>
    <link href="./css/app.4d00a7f1.css" rel="stylesheet">
</head>

<body><noscript><strong>We're sorry but clock doesn't work properly without JavaScript enabled. Please enable it to
            continue.</strong></noscript>
    <div id="app"></div>
</body>

</html>
```

##### 增加plugin.json文件输入内容

```json
{
	"development":{
        "main":"http://192.168.60.152:8080/"
    },
	"logo": "logo.png",
	"features": [
		{
		  "code": "clock",
		  "explain": "翻页时钟",
			"cmds":["翻页", "时钟"]
		}
	]
}

```

> "development":{
>         "main":"http://192.168.60.152:8080/"
>     }//可以修改为"main":"index.html"
>
> 这个是开发模式下的，后面把plugin.json在utools开发者工具运行时，可以根据项目的修改改变样式。但是地址要对
>
> 打包好的index.html可以直接放在“main"里面，这样的样式就固定了